<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/about','SiteController@aboutPage');
Route::get('/service','SiteController@servicePage');

Route::get('/contact', function () {
    return view('contact');
});

Route::post('/contact-send','ContactUsFormController@ContactUsForm');



Auth::routes();

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

Route::view('/admin/home', 'home')->middleware('auth');
Route::view('/admin', 'admin.index');


Route::group(['prefix' => 'admin','middleware' => ['auth']],function() {
    Route::get('/home/page','Admin\Pages\HomeController@index');
    Route::get('/home/page/create','Admin\Pages\HomeController@create');
    Route::post('/home/page/top-header','Admin\Pages\HomeController@TopBarCreate')->name('admin.home-top-header-store');
   
    Route::get('/home/page/edit/{id}','Admin\Pages\HomeController@edit');
    Route::post('/home/page/update/{id}','Admin\Pages\HomeController@TopBarUpdate');
    
    Route::post('/sitelogo','Admin\Pages\HomeController@siteLogoCreate')->name('create-sitelogo');
    Route::post('/home/sitelog/update/{id}','Admin\Pages\HomeController@siteLogoUpdate');

    Route::get('/about','Admin\Pages\AboutController@index');
    Route::get('/about/create','Admin\Pages\AboutController@create');
    Route::post('/about/create','Admin\Pages\AboutController@store')->name('admin.about-store');
    Route::get('/about/edit/{id}','Admin\Pages\AboutController@edit')->name('admin.about-edit');
    Route::post('/about/update/{id}','Admin\Pages\AboutController@update')->name('admin.about-update');

    Route::get('/service','Admin\Pages\ServiceController@index');
    Route::get('/service/create','Admin\Pages\ServiceController@create');
    Route::post('/service/create','Admin\Pages\ServiceController@store')->name('admin.service-store');

    Route::get('/slider','Admin\Pages\SliderController@index');
    Route::get('/slider/create','Admin\Pages\SliderController@create');
    Route::post('/slider/create','Admin\Pages\SliderController@store')->name('admin.slider-store');

    Route::get('/contact','Admin\Pages\ContactController@index');
    
 
});



