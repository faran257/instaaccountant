<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\ImageUploadTrait;
use App\About;
use Image;

class AboutController extends Controller
{
    use ImageUploadTrait;
    protected $photo = 'photo';

    public function index()
    {
        $abouts = About::all();
        return view('admin.pages.about.index',compact('abouts'));
    }
    public function create()
    {
        return view('admin.pages.about.create');
    }

    public function store(Request $request)
    {
        //dd($request);
        $validate = Validator::make($request->all(), [
            'photo' => 'required',
            'title' => 'required',
            'content'=>'required'
         ])->validate();
         $abouts = About::create([
            'name'=>$request->title,
            'image'=>  $this->uploadImage($request->photo),
            'title'=>$request->title,
            'content'=>$request->content
         ]);
         if ($abouts) {
            return redirect('admin/about');
         }
       
    }
    
    public function edit($id)
    {
        $abouts = About::where('id',$id)->first();
        return view('admin.pages.about.edit',compact('abouts'));
    }

    public function update(Request $request, $id)
    {
        //dd($request);
        $validate = Validator::make($request->all(), [
            'photo' => 'required',
            'title' => 'required',
            'content'=>'required'
         ])->validate();
         $abouts = About::where('id',$id)->update([
            'name'=>$request->title,
            'image'=>  $this->uploadImage($request->photo),
            'title'=>$request->title,
            'content'=>$request->content
         ]);
         if ($abouts) {
            return redirect('admin/about');
         }
       
    }
}
