<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\ImageUploadTrait;
use App\Service;

class ServiceController extends Controller
{
    use ImageUploadTrait;
    protected $photo = 'photo';

    public function index()
    {
        $services = Service::all();
        return view('admin.pages.service.index',compact('services'));
    }
    public function create()
    {
        return view('admin.pages.service.create');
    }

    public function store(Request $request)
    {
        //dd($request);
        $validate = Validator::make($request->all(), [
            'icon' => 'required',
            'title' => 'required',
            'content'=>'required'
         ])->validate();
         $services = Service::create([
            'name'=>$request->title,
            'image'=>$request->icon,
            'title'=>$request->title,
            'content'=>$request->content
         ]);
         if ($services) {
            return redirect('admin/service');
         }
       
    }
}
