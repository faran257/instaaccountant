<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topbar extends Model
{
    //
    protected $fillable =['top_left_header','right_left_header',];
}
