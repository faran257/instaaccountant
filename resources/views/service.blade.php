@extends('layouts.master')

@section('title','Services')
@section('content')
	
	
	<!-- Page top section  -->
	<section class="page-top-section set-bg" data-setbg="img/milestones-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<h2>Our Services</h2>
				
				</div>
			</div>
		</div>
	</section>
	<!-- Page top section end  -->

	<!-- Services section  -->
	<section class="services-section">
		<div class="services-warp">
			<div class="container">
				<div class="row">
				@foreach($services as $service)
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<i class="{{$service->image}}"></i>
								</div>
								<h5>{{$service->title}}</h5>
							</div>
							<p>{{$service->content}}</p>
						</div>
					</div>
				@endforeach	
				</div>
			</div>
		</div>
	</section>
	<!-- Services section end  -->


	<!-- Reserch section  -->
	
	<!-- Reserch section end  -->

	<!-- Call to action section  -->
	<!-- Call to action section end  -->

@endsection