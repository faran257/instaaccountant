@extends('layouts.master')
@section('title','Contact')
@section('content')
	
	<!-- Page top section  -->
	<section class="page-top-section set-bg" data-setbg="img/milestones-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<h2>Contact</h2>
					
				</div>
			</div>
		</div>
	</section>
	<!-- Page top section end  -->

	<!-- Map section  -->
	<!-- <div class="map-section">
		<div class="container">
			<div class="map-info">
				<img src="img/logo-contact.png" alt="">
				<p>Lorem ipsum dolor sit amet, consec-tetur adipiscing elit. Quisque orci purus, sodales in est quis, blandit sollicitudin est. Nam ornare ipsum ac accumsan auctor. </p>
			</div>
		</div>
		<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe>
		</div>
	</div> -->
	<!-- Map section end  -->

	<!-- Contact section   -->
	<section class="contact-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="contact-text">
						<h2>Get in Touch</h2>
						<p>Insta Accountants provides a wide range business, accounting and tax services to clients operating in diverse industries.</p>
						<div class="header-info-box">
							<div class="hib-icon">
								<i class="fa fa-phone"></i>
							</div>
							<div class="hib-text">
								<h6>0115 784 6363</h6>
								<p>info@instaaccountants.co.uk</p>
							</div>
						</div>
						<div class="header-info-box">
							<div class="hib-icon">
								<i class="fa fa-location-arrow"></i>
							</div>
							<div class="hib-text">
								<h6>221 Radford Road,</h6>
								<p>Nottingham, NG7 5GT</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
				@if(Session::has('message'))
						<div class="alert alert-success">
							{{Session::get('message')}}
						</div>
					@endif
					@if(Session::has('error'))
						<div class="alert alert-success">
							<ul>
								<li>{{Session::get('error')}}</li>
							</ul>
						</div>
					@endif
					<form  method="POST" action="{{url('contact-send')}}" class="contact-form">
						@csrf
						<div class="row">
							<div class="col-lg-6">
								<input type="text" placeholder="Your Name" name="name">
							</div>
							<div class="col-lg-6">
								<input type="email" placeholder="Your Email" name="email">
							</div>
							<div class="col-lg-4">
							</div>
							<div class="col-lg-12">
								<input type="text" placeholder="Subject" name="subject">
								<textarea class="text-msg" placeholder="Message" name="message"></textarea>
								<button class="site-btn btn-danger" type="submit">send message</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Contact section end  -->

@endsection




