@extends('layouts.master')
@section('title','Home')
@section('content')
	<!-- Hero section  -->
	<section class="hero-section">
		<div class="hero-slider owl-carousel">
			<div class="hero-item set-bg" data-setbg="img/Accounting.png">
				<div class="container">
					<div class="row">
						<div class="col-xl-8">
							<h2><span>GROW</span><span>YOUR</span><span>BUSINESS</span></h2>
							<a href="{{url('service')}}" class="site-btn sb-white mr-4 mb-3">Read More</a>
							<a href="{{url('service')}}" class="site-btn sb-dark">our Services</a>
						</div>
					</div>
				</div>
			</div>
			<div class="hero-item set-bg" data-setbg="img/hero-slider/3.jpg">
				<div class="container">
					<div class="row">
						<div class="col-xl-8">
						    <h2><span>GROW</span><span>YOUR</span><span>BUSINESS</span></h2>
							<a href="{{url('service')}}" class="site-btn sb-white mr-4 mb-3">Read More</a>
							<a href="{{url('service')}}" class="site-btn sb-dark">our Services</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end  -->

	<!-- Services section  -->
	<section class="services-section">
		<div class="services-warp">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
									<i class="fa fa-book"></i>
								</div>
								<h5>Bookkeeping services </h5>
							</div>
							<p>Whether you looking to outsource your bookkeeping hundred per cent, 
								need a one-off bookkeeping service, or a just a cloud-based accounting software (QuickBooks) to stay on top of your accounts, 
								</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
								   <i class="fa fa-calculator"></i>
								</div>
								<h5>Annual Accounts</h5>
							</div>
							<p>Our chartered certified Accountants are experts in preparing annual accounts for a variety of businesses such as sole traders, partnerships and limited companies.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
								  <i class="fa fa-book"></i>
								</div>
								<h5>QuickBooks Accounting </h5>
							</div>
							<p>If you are looking for accounting software for meet 
								your bookkeeping needs such as to records business expenses, raise sales invoices and keep your finances under a tight check</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
								<i class="fa fa-gbp"></i>
								</div>
								<h5>Payroll service</h5>
							</div>
							<p>We offer a highly competitive, bespoke and reliable
								 payroll solution to your business requirements that is fully compliant to current HMRC legislation. You have come to the right place. </p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="service-item">
							<div class="si-head">
								<div class="si-icon">
								<i class="fa fa-book"></i>
								</div>
								<h5>Self-Assessment Tax Returns</h5>
							</div>
							<p>If you are setting up a new business as a sole trader, planning to acquire a running business and already have an established business,
								 you need to prepare annual accounts to complete your self-assessment tax</p>
						</div>
					</div>
					<!--<div class="col-lg-4 col-md-6">-->
					<!--	<div class="service-item">-->
					<!--		<div class="si-head">-->
					<!--			<div class="si-icon">-->
					<!--				 <i class="fa fa-calculator"></i>-->
					<!--			</div>-->
					<!--			<h5>Power & Energy</h5>-->
					<!--		</div>-->
					<!--		<p>Sodales in est quis, blandit sollicitudin est. Nam ornare ipsum ac accumsan auctor. Donec con-sequat arcu et commodo interdum. </p>-->
					<!--	</div>-->
					<!--</div>-->
				</div>
			</div>
		</div>
	</section>
	<!-- Services section end  -->

	  <!-- Milestones section -->
	  <section class="milestones-section set-bg" data-setbg="img/milestones-bg.jpg">
		<!-- <div class="container text-white">
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="milestone">
						<div class="milestone-icon">
							<img src="img/icons/plug.png" alt="">
						</div>
						<div class="milestone-text">
							<span>Clients</span>
							<h2>725</h2>
							<p>Nam ornare ipsum </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="milestone">
						<div class="milestone-icon">
							<img src="img/icons/light.png" alt="">
						</div>
						<div class="milestone-text">
							<span>Growth</span>
							<h2>45%</h2>
							<p>Nam ornare ipsum </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="milestone">
						<div class="milestone-icon">
							<img src="img/icons/traffic-cone.png" alt="">
						</div>
						<div class="milestone-text">
							<span>Projects</span>
							<h2>59</h2>
							<p>Nam ornare ipsum </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="milestone">
						<div class="milestone-icon">
							<img src="img/icons/worker.png" alt="">
						</div>
						<div class="milestone-text">
							<span>Emploees</span>
							<h2>138</h2>
							<p>Nam ornare ipsum </p>
						</div>
					</div>
				</div>
			</div>
		</div> -->
	</section>
	<!-- Milestones section end -->
	
	<!-- Video section  -->
	<section class="video-section spad" >
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="video-text">
						<h2>We do our best to </h2>
						<ul>
							<li>Offer you a service tailored to your unique needs</li>
							<li>Minimise you tax liability – no-one enjoy paying too much tax, we will sure you don’t have to pay a penny extra</li>
					        <li>Minimise you tax liability – no-one enjoy paying too much tax, we will sure you don’t have to pay a penny extra</li>
						    <li>Treat your information with strictly confidentially, do not share it with anyone and only use it to complete your tax return</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6">
					<img src="img/Accounting.png" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- Video section end  -->


	<!-- Testimonial section -->
	<section class="testimonial-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 p-0">
					<div class="testimonial-bg set-bg" data-setbg="img/Accounting.png"></div>
				</div>
				<div class="col-lg-6 p-0">
					<div class="testimonial-box">
						<div class="testi-box-warp">
							<h2>Client’s Testimonials</h2>
							<div class="testimonial-slider owl-carousel">
								<div class="testimonial">
									<p>Insta Accountants provides a wide range business, accounting and tax services to clients operating in diverse industries. Whether you are running your business as limited company, partnership or sole trader, we’ve got the knowledge and expertise to professional serve your business needs.</p>
									<!--<img src="img/testimonial-thumb.jpg" alt="" class="testi-thumb">-->
									<!--<div class="testi-info">-->
									<!--	<h5>Michael Smith</h5>-->
									<!--	<span>CEO Industrial INC</span>-->
									<!--</div>-->
								</div>
								<div class="testimonial">
									<p>Insta Accountants provides a wide range business, accounting and tax services to clients operating in diverse industries. Whether you are running your business as limited company, partnership or sole trader, we’ve got the knowledge and expertise to professional serve your business needs.</p>
									<!--<img src="img/testimonial-thumb.jpg" alt="" class="testi-thumb">-->
									<!--<div class="testi-info">-->
									<!--	<h5>Michael Smith</h5>-->
									<!--	<span>CEO Industrial INC</span>-->
									<!--</div>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial section end  -->


	<!-- Call to action section  -->
	<section class="cta-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 d-flex align-items-center">
					<h2>We provide accounting and tax solutions to your business</h2>
				</div>
				<div class="col-lg-3 text-lg-right" >
					<a href="{{url('contact')}}" class="site-btn sb-dark">contact us</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Call to action section end  -->

	
	<!-- Features section   -->
	<section class="features-section spad set-bg" data-setbg="img/features-bg.jpg">
		<div class="container">
		<div class="client-text">
				<h2 class="text-white">Recent Clients</h2>
				<p>Insta Accountants provides a wide range business, accounting and tax services to clients operating in diverse industries. Whether you are running your business as limited company, partnership or sole trader, we’ve got the knowledge and expertise to professional serve your business needs.</p>
			</div>
			<div id="client-carousel" class="client-slider owl-carousel">
				<div class="single-brand">
					<a href="#">
						<img src="img/clients/1.png" alt="">
					</a>
				</div>
				<div class="single-brand">
					<a href="#">
						<img src="img/clients/2.png" alt="">
					</a>
				</div>
				<div class="single-brand">
					<a href="#">
						<img src="img/clients/3.png" alt="">
					</a>
				</div>
				<div class="single-brand">
					<a href="#">
						<img src="img/clients/4.png" alt="">
					</a>
				</div>
				<div class="single-brand">
					<a href="#">
						<img src="img/clients/5.png" alt="">
					</a>
				</div>

		</div>
	</section>
	<!-- Features section end  -->
   
@endsection