<?php

namespace App\Traits;
use Image;

trait ImageUploadTrait
{
    public function uploadImage($query)
    {
        $ext = strtolower($query->getClientOriginalExtension()); // You can use also getClientOriginalName()
        $image_full_name = time().'.'.$ext;
        $upload_path = 'upload/';    //Creating Sub directory in Public folder to put image
        $image_url = $upload_path.$image_full_name;
        $success = $query->move($upload_path,$image_full_name);
 
        return $image_url; // Just return image
    }

    
}