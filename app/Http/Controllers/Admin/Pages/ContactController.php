<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    /**
     * Contact 
     */
    public function index()
    {
        $contacts = Contact::all();
        return view('admin.pages.contact.index',compact('contacts'));
    }


}
