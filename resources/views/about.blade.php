@extends('layouts.master')
@section('title','About')
@section('content')
	<!-- Page top section  -->
	<section class="page-top-section set-bg" data-setbg="img/milestones-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<h2>About us</h2>
				</div>
			</div>
		</div>
	</section>
	<!-- Page top section end  -->


		<!-- About section -->
        <section class="about-section spad">
		<div class="container">
			<div class="row">
			@foreach($abouts as $about)
				<div class="col-lg-6">
					<img src="../{{$about->image}}"  height="600" alt="">
				</div>
				<div class="col-lg-6">
					<div class="about-text">
						<h2>{{$about->title}}</h2>
						<p>{{$about->content}}</p>
						<div class="about-sign">
							<div class="sign">
								<!-- <img src="img/sign.png" alt=""> -->
							</div>
							<div class="sign-info">
								<h5>Azhar Aziz</h5>
								<span>Chartered Certified Accountant (ACCA)</span>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			</div>
		</div>
	</section>
	<!-- About section end -->

	@endsection