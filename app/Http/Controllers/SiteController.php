<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use App\Service;

class SiteController extends Controller
{
    /**
     * About Page from database
    */
    public function aboutPage()
    {
        $abouts = About::all();
        return view('about',compact('abouts'));
    }
    /***
     * Services Page 
     */
    public function servicePage()
    {
        $services = Service::all();
        return view('service',compact('services'));
    }
   
}
