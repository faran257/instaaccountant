<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta name="description" content="Industry.INC HTML Template">
	<meta name="keywords" content="industry, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i&display=swap" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/slicknav.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.min.css"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css"/>


	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section  -->
	<header class="header-section clearfix">
	    <div class="header-top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<p><b>INSTA ACCOUNTANTS LTD.</b></p>
					</div>
					<div class="col-md-6 text-md-right">
					<p><b>Chartered Certified Accountants</b></p>
					</div>
				</div>
			</div>
		</div>
		<div class="site-navbar">
			<!-- Logo -->
			<a href="index.html" class="site-logo">
				<img src="img/logo2.png" alt="">
			</a>
			<div class="header-right">
				<div class="header-info-box">
					<div class="hib-icon">
						<i class="fa fa-phone"></i>
					</div>
					<div class="hib-text">
						<h6>0115 784 6363</h6>
						<p>info@instaaccountants.co.uk</p>
					</div>
				</div>
				<div class="header-info-box">
					<div class="hib-icon">
						<i class="fa fa-location-arrow"></i>
					</div>
					<div class="hib-text">
						<h6>221 Radford Road,</h6>
						<p>Nottingham, NG7 5GT</p>
					</div>
				</div>
				<!-- <button class="search-switch"><i class="fa fa-search"></i></button> -->
			</div>
			<!-- Menu -->
			<nav class="site-nav-menu" id="nav">
				<ul>
					<li class="active"><a href="{{url('/')}}">Home</a></li>
					<li><a href="{{url('about')}}">About</a></li>
					<li  ><a href="{{url('service')}}">Services</a>
					</li>
					<li><a href="{{url('contact')}}">Contact</a></li>
				</ul>
			</nav>

		</div>
	</header>
    <!-- Header section end  -->
    

    @yield('content')


    <!-- Footer section -->
	<footer class="footer-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="footer-widget about-widget">
					<h2 class="fw-title"><b>Accredited by</b></b></h2>
					   <img src="img/acca.jpg" alt="">
						<div class="footer-social">
							<a href=""><i class="fa fa-facebook"></i></a>
							<a href=""><i class="fa fa-twitter"></i></a>
							<a href=""><i class="fa fa-dribbble"></i></a>
							<a href=""><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="footer-widget">
						<h2 class="fw-title"><b>Useful Resources</b></b></h2>
						<ul>
					    	<li class="active"><a href="{{url('/')}}">Home</a></li>
							<li><a href="{{url('about')}}">About</a></li>
							<li><a href="{{url('service')}}">Services</a>
								<!-- <ul class="sub-menu">
									<li><a href="elements.html">Elements</a></li>
								</ul> -->
							</li>
							<li><a href="{{url('contact')}}">Contact</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="footer-widget">
						<h2 class="fw-title"><b>Our Solutions</b></h2>
						<ul>
				     		<li><a href="{{url('service')}}">Bookkeeping services</a></li>
							<li><a href="{{url('service')}}">Annual Accounts</a></li>
							<li><a href="{{url('service')}}">QuickBooks Accounting</a></li>
							<li><a href="{{url('service')}}">Payroll service</a></li>
							<li><a href="{{url('service')}}">Self-Assessment Tax Returns</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-7">
					<div class="footer-widget text-white">
						<h2 class="fw-title"><b>Contact Us</b></h2>
						<div class="footer-info-box">
							<div class="fib-icon">
								<i class="fa fa2 fa-location-arrow"></i>
							</div>
							<div class="fib-text">
								<p>221 Radford Road,<br> Nottingham, NG7 5GT</p>
							</div>
						</div>
						<div class="footer-info-box">
							<div class="fib-icon">
							   <i class="fa fa2 fa-phone"></i>
							</div>
							<div class="fib-text">
								<p>0115 784 6363<br>info@instaaccountants.co.uk</p>
							</div>
						</div>
						<form class="footer-search">
							<input type="text" placeholder="Search">
							<button><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		</div>
	</footer>
	<!-- Footer section end -->

	<!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->
	<div class="bottom-bar">
        <div class="container">
			<div class="row">
				 <div class="col-md-12">
				 <p class="text-white">&copy Copyright Insta Accountan LTD | Company Number 12309623 | Term & Conditons</p>
				 </div>
			</div>
		</div>
	</div>
	<!--====== Javascripts & Jquery ======-->
	<script src="{{url('js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{url('js/bootstrap.min.js')}}"></script>
	<script src="{{url('js/jquery.slicknav.min.js')}}"></script>
	<script src="{{url('js/owl.carousel.min.js')}}"></script>
	<script src="{{url('js/circle-progress.min.js')}}"></script>
	<script src="{{url('js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{url('js/main.js')}}"></script>
   
	</body>
</html>
