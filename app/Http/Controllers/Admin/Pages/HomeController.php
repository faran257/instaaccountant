<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\ImageUploadTrait;
use App\Topbar;
use App\SiteLogo;

class HomeController extends Controller
{
    use ImageUploadTrait;
    protected $photo = 'photo';

    public function index()
    {
        $topheaders = TopBar::all();
        $sitelogos   = SiteLogo::all();
        return view('admin.pages.home.index',compact('topheaders','sitelogos'));
    }
    public function create()
    {
        return view('admin.pages.home.create');
    }

    public function TopBarCreate(Request $request)
    {
        
        $validate = Validator::make($request->all(), [
            'left_header' => 'required',
            'right_header' => 'required'
         ])->validate();
         $homes = Topbar::create([
            'top_left_header' => $request->left_header,
            'right_left_header' => $request->right_header
         ]);
         if ($homes) {
             return back();
         }
    }
   

    public function TopBarUpdate(Request $request, $id)
    {
        
        $validate = Validator::make($request->all(), [
            'left_header' => 'required',
            'right_header' => 'required'
         ])->validate();
         $homes = Topbar::where('id',$id)->update([
            'top_left_header' => $request->left_header,
            'right_left_header' => $request->right_header
         ]);
         if ($homes) {
             return back();
         }
    }
   

     public function edit($id) 
     {
        $topheaders = TopBar::where('id',$id)->first();
        $sitelogos   = SiteLogo::where('id',$id)->first();
        return view('admin.pages.home.edit',compact('topheaders','sitelogos'));
     }
    public function siteLogoCreate(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'photo' => 'required'
         ])->validate();
         $homes = SiteLogo::create([
            'image' => $this->uploadImage($request->photo)
         ]);
         if ($homes) {
             return back();
         }
    }

    public function siteLogoUpdate(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'photo' => 'required'
         ])->validate();
         $homes = SiteLogo::where('id',$id)->update([
            'image' => $this->uploadImage($request->photo)
         ]);
         if ($homes) {
             return back();
         }
    }
}
