<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteLogo extends Model
{
    protected $fillable = ['image',];
}
