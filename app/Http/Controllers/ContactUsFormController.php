<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contact;
use Mail;

class ContactUsFormController extends Controller
{
     // Store Contact Form data
     public function ContactUsForm(Request $request) {

        // Form validation
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'subject'=>'required',
            'message' => 'required'
         ])->validate();
         
        //  Store data in database
        Contact::create($request->all());
        
         //  Send mail to admin
         \Mail::send('mail', array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'user_query' => $request->get('message'),
        ), function($message) use ($request){
            $message->from($request->email);
            $message->to('azharmogal@gmail.com', 'Admin')->subject($request->get('subject'));
        });

        return back()->with('message', 'We have received your message and would like to thank you for contacting to us.');
    }

}
