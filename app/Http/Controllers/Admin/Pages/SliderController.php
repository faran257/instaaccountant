<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\ImageUploadTrait;
use App\Slider;

class SliderController extends Controller
{
    use ImageUploadTrait;
    protected $photo = 'photo';

    public function index()
    {
        $sliders = Slider::all();
        return view('admin.pages.slider.index',compact('sliders'));
    }
    public function create()
    {
        return view('admin.pages.slider.create');
    }

    public function store(Request $request)
    {
        //dd($request);
        $validate = Validator::make($request->all(), [
            'photo' => 'required',
            'title' => 'required',
         ])->validate();
         $abouts = Slider::create([
            'image'=>  $this->uploadImage($request->photo),
            'title'=>$request->title
         ]);
         if ($abouts) {
            return redirect('admin/slider');
         }
       
    }
}
